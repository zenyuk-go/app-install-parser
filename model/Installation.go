package model

// Installation - single installed application on a computer type is desktop and source of the information
type Installation struct {
	Desktop bool
	Source  rune
}
