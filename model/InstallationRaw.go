package model

// CompID - computer ID
type CompID int

// InstallationRaw - not structured parsed data
type InstallationRaw struct {
	ComputerID    CompID
	ApplicationID AppID
	Desktop       bool
	Source        rune

	// Ignoring UserID as computers with multiple users are not supported
	//UserID int
}
