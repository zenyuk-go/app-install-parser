package main

import (
	"testing"

	"gitlab.com/zenyuk_go/app-install-parser/model"
)

func TestAggregateInstallation(t *testing.T) {
	// arrange
	const compID = model.CompID(123)
	const appID = model.AppID(456)
	const source = rune('A')
	raw := model.InstallationRaw{
		ComputerID:    compID,
		ApplicationID: appID,
		Desktop:       true,
		Source:        source,
	}

	all := allComputers{}

	// act
	aggregateInstallation(&raw, &all)

	// assert
	c, ok := all[compID]
	if !ok {
		t.Error("computer by ID - not found")
	}

	installations, ok := c[appID]
	if !ok {
		t.Error("application by ID - not found")
	}

	if !installations[0].Desktop {
		t.Error("incorrect computer type")
	}

	if installations[0].Source != source {
		t.Error("incorrect export source")
	}
}

func TestParseInstallation(t *testing.T) {
	// arrange
	source := "167258,8143,235,DESKTOP,Exported from System B"

	// act
	result, err := parseInstallation(source)

	// assert
	if err != nil {
		t.Errorf("error parsing single line %v", err)
	}

	if result.ComputerID != 167258 {
		t.Error("wrong computer ID")
	}

	if result.ApplicationID != 235 {
		t.Error("wrong application ID")
	}

	if !result.Desktop {
		t.Error("wrong computer type")
	}

	if result.Source != rune('B') {
		t.Error("wrong exported source")
	}
}

func TestCountLicenses(t *testing.T) {
	// arrange
	const targetAppID = 123
	allComps := allComputers{
		1: model.ComputerInstallations{
			targetAppID: model.AppInstallations{
				model.Installation{
					Desktop: true,
					Source:  'A',
				},
			},
		},
		2: model.ComputerInstallations{
			targetAppID: model.AppInstallations{
				model.Installation{
					Desktop: false,
					Source:  'B',
				},
			},
		},
		3: model.ComputerInstallations{
			targetAppID: model.AppInstallations{
				model.Installation{
					Desktop: false,
					Source:  'A',
				},
			},
		},
		4: model.ComputerInstallations{
			targetAppID: model.AppInstallations{
				model.Installation{
					Desktop: false,
					Source:  'B',
				},
			},
		},
		5: model.ComputerInstallations{
			125: model.AppInstallations{
				model.Installation{
					Desktop: false,
					Source:  'B',
				},
			},
		},
	}

	// act
	result := countLicenses(allComps)

	// assert
	if len(result) != 2 {
		t.Error("countLicenses - incorrect number of applications")
	}
	if result[targetAppID] != 3 {
		t.Error("countLicenses - incorrect number of licenses")
	}
}

func TestMergeLaptopsAndDesktops_twoLaptopsAndDesktop(t *testing.T) {
	// arrange
	targetAppID := model.AppID(123)
	randomAppID := model.AppID(456)
	laptops := map[model.AppID]int{
		targetAppID: 2,
		randomAppID: 1,
	}
	desktops := map[model.AppID]int{
		targetAppID: 1,
		randomAppID: 5,
	}

	// act
	result := mergeLaptopsAndDesktops(laptops, desktops)

	// assert
	count, ok := result[targetAppID]
	if !ok {
		t.Error("can not merge laptops and desktops - Application ID, not found")
	}
	if count != 2 {
		t.Error("can not merge laptops and desktops - incorrect result for 2 laptops + 1 desktop")
	}
}
